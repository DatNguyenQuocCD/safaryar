import { memo } from "react";
import Image from "next/image";
import classNames from "classnames";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";

import StarRating from "../starRating";

interface IProps {
  boxClassName?: string | undefined;
  item?: any
}

const ItemFood: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.item

  return (
    <div className={classNames("activity-box", props?.boxClassName)}>
      <div className="activity-card">
        <div className="act-img-sc">
          <div className="hd-col">
            <div className="originals-new">
              <Image
                className="inline mr-2 w-3"
                src={require('../../../public/images/og.png')}
                width={100}
                height={100}
                alt="none"
              />
              Originals by Safaryar
            </div>
            <FontAwesomeIcon className="wishlist-ic" icon={faHeart} size="xl" color="#fffff" />
          </div>
          <Image
            src={item?.image}
            alt="none"
          />
        </div>
        <div className="act-content">
          <span className="activity-type">Guided tour</span>
          <h3 className="activity-card-title">
            {item?.name}
          </h3>
          <ul className="act-attributes__con">
            <li className="activity-attr">
              <span className="bullet">{item?.hours} hours</span>
            </li>
            <li className="activity-attr">
              <span>Small group</span>
            </li>
          </ul>

          <div className="star-sc">
            <div className="stars">
              <StarRating rating={item?.star} />
            </div>
            {item?.star} <span>{item?.review} reviews</span>
          </div>

          <div className="act-frm">
            <span> From ₹ {item?.from} </span> per person
          </div>
        </div>
      </div>
    </div>
  )
}

export default memo(ItemFood)
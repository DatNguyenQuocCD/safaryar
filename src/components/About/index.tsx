import { memo } from "react";
import StarRating from "../starRating";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarCheck, faCalendarDay, faClockRotateLeft, faForward, faWheelchair } from "@fortawesome/free-solid-svg-icons";

interface IProps {
  adventures?: any;
}

const About: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.adventures;
  return (
    <div>
      <h2 className="text-[25px] mb-[35px]">About this activity</h2>
      <div className="activity-section">
        <div className="activity-container">
          <FontAwesomeIcon icon={faCalendarCheck} />
        </div>
        <div className="activity-content">
          <h2 className="activity-heading">Free cancellation</h2>
          <p className="activity-description">
            Cancel up to 24 hours in advance for a full refund
          </p>
        </div>
      </div>

      <div className="activity-section nowr">
        <div className="activity-container">
          <FontAwesomeIcon icon={faCalendarDay} />
        </div>
        <div className="activity-content">
          <h2 className="activity-heading">Reserve now & pay later</h2>
          <p className="activity-description">
            Keep your travel plans flexible — book your spot and pay nothing
            today.
          </p>
        </div>
      </div>

      <div className="activity-section">
        <div className="activity-container">
          <FontAwesomeIcon icon={faClockRotateLeft} />
        </div>
        <div className="activity-content">
          <h2 className="activity-heading">Valid 30 minutes</h2>
          <p className="activity-description">from first activation</p>
        </div>
      </div>

      <div className="activity-section">
        <div className="activity-container">
          <FontAwesomeIcon icon={faForward} />
        </div>
        <div className="activity-content">
          <h2 className="activity-heading">Skip the ticket line</h2>
          <p></p>
        </div>
      </div>

      <div className="activity-section">
        <div className="activity-container">
          <FontAwesomeIcon icon={faWheelchair} />
        </div>
        <div className="activity-content">
          <h2 className="activity-heading">Wheelchair accessible</h2>
          <p></p>
        </div>
      </div>
    </div>
  );
};

export default memo(About);

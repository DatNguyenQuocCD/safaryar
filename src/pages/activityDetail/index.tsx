import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Breadcrumbs from "../../components/Breadcrumbs";
import ServiceHeader from "../../components/Header/ServiceHeader";
import Gallery from "../../components/carouselCustom/gallery";
import {
  faCalendarCheck,
  faCalendarDay,
  faClockRotateLeft,
  faForward,
  faWheelchair,
} from "@fortawesome/free-solid-svg-icons";
import About from "../../components/About";
import ReservationBox from "../../components/Reservation";

export default function ActivityDetail() {
  const Links = [
    {
      id: 1,
      title: "United States",
    },
    {
      id: 1,
      title: "Illinois",
    },
    {
      id: 1,
      title: "Things to do in Chicago",
    },
    {
      id: 1,
      title: "Willis Tower",
    },
  ];

  const slide = [
    {
      id: 1,
      img: "images/ds1.jpg",
    },
    {
      id: 2,
      img: "images/ds2.jpg",
    },
    {
      id: 3,
      img: "images/ds3.jpg",
    },
    {
      id: 4,
      img: "images/ds4.jpg",
    },
    {
      id: 5,
      img: "images/ds5.jpg",
    },
  ];

  const header = {
    title: "Third Reich and Cold War Walking Tour",
    star: 4.5,
    reviews: 1.543,
    location: "Skydeck Chicago",
  };
  return (
    <div className="flex flex-col justify-center items-center">
      <div className="w-[1320px]">
        <Breadcrumbs links={Links} />
        <ServiceHeader item={header} />
        <Gallery images={slide} />
        <div className="flex">
          <div>
            <p className="mb-[40px]">
              Treat yourself to an exhilarating view over the whole of Chicago
              from the top of the Willis Tower. Ride 103 floors to the Skydeck
              and step out onto The Ledge, a glass-paneled box with vertical
              views to the ground below.
            </p>
            <About />
            <ReservationBox />
            <div className="reservation-box-result">
              <div className="reservation-box-hder">
                <h3>New York City: Madison Square Garden Tour Experience</h3>
                <div className="cart-details res">
                  <ul>
                    <li>
                      <i className="fa-solid fa-clock-rotate-left"></i>1 hour
                    </li>
                    <li>
                      <i className="fa-regular fa-flag"></i>
                      Guide: English
                    </li>
                    <li>
                      <i className="fa-solid fa-location-dot"></i>
                      Meet at 404-408 Fashion Ave, New York, NY 10001, USA
                    </li>
                  </ul>
                </div>
              </div>

              <div className="reservation-box-middle">
                <div className="gift-form setting-personal-details">
                  <div className="single-inpt">
                    <label>Select a starting time</label>
                    <div className="d-flex dew grid grid-cols-4">
                      <div className="divin">
                        <input type="radio" name="time_slot" value="10:30 AM" />
                        10:30 AM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="11:00 AM" />
                        11:00 AM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="11:30 AM" />
                        11:30 AM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="12:00 PM" />
                        12:00 PM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="12:30 PM" />
                        12:30 PM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="1:00 PM" />
                        1:00 PM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="1:30 PM" />
                        1:30 PM
                      </div>
                      <div className="divin">
                        <input type="radio" name="time_slot" value="2:00 PM" />
                        2:00 PM
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div className="reservation-box-middle">
                <div className="containert">
                  <div className="left-half"></div>
                  <div className="right-half">
                    <div className="price-breakdown">
                      <p> Price breakdown</p>
                      <small> Adult 4 × ₹ 3,814</small>
                    </div>
                    <div className="total-price">
                      <small> ₹ 15,256</small>
                    </div>
                  </div>
                </div>
              </div>

              <div className="reservation-box-footer">
                <div className="product-details">
                  <div className="left-section">
                    <div className="total-price">
                      <small> Total price</small>
                      <h4>₹ 15,256</h4>
                      <small>All taxes and fees included</small>
                    </div>
                  </div>
                  <div className="right-section">
                    <button className="add-to-cart">Add to cart</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="w-[800px]">
            <p>layout</p>
          </div>
        </div>
      </div>
    </div>
  );
}

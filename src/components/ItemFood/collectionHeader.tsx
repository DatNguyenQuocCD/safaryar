import { memo } from "react";
import classNames from "classnames";

interface IProps {
  cardP?: string;
  cardH2?: String
  className?: string
}

const CollectionHeader: React.FC<IProps> = ({ ...props }: IProps) => {

  return (
    <div className={classNames('collection-header_con', props?.className)}>
      <p>{props?.cardP}</p>
      <h2 className="collection-header_title">
        {props?.cardH2}
      </h2>
    </div>
  )
}

export default memo(CollectionHeader)
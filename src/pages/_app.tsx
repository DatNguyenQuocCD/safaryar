import { AppProps } from "next/app";
import Layout from "../components/Layout";
import "../styles/globals.css";
import "../styles/styles.css";
import "../styles/slideShow.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
};

export default MyApp;

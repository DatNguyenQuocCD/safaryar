import { memo } from "react";
import StarRating from "../starRating";

interface IProps {
  adventures?: any;
}

const Adventures: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.adventures;
  return (
    <div className="destination-box">
      <a className="location-card" href="#">
        <figure className="image-figure">
          <img src={item?.img} alt="photo" />
          <figcaption className="location-card_caption">
            <div className="intro_center text-center">
              <h2>{item?.title}</h2>
              <div className="intro_price">From ${item?.price}</div>
              <div className="adventures-rating">
                <StarRating rating={item?.star} />
              </div>
            </div>
          </figcaption>
        </figure>
      </a>
    </div>
  );
};

export default memo(Adventures);

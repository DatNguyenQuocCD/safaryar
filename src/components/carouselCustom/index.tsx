import { memo } from "react";
import classNames from "classnames";
import { Carousel } from "@trendyol-js/react-carousel";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/free-solid-svg-icons";

interface IProps {
  renderItem?: any
  show: number
  slide: number
  className?: string
  showIcon?: boolean
}

const TStyle = {
  arrow: "h-full flex justify-center items-center cursor-pointer ml-[10px] max-md:hidden max-lg:hidden",
  arrowLeft: "h-full flex justify-center items-center cursor-pointer mr-[10px] max-md:hidden max-lg:hidden"
}

const CarouselCustom: React.FC<IProps> = ({ ...props }: IProps) => {

  return (
    <div className={props?.className}>
      <Carousel show={Number(props?.show) - 0.008} swipeOn={0.5} autoSwipe={0.5} slide={props?.slide} swiping={true} infinite
        leftArrow={
          <div className={classNames(props?.showIcon ? TStyle.arrowLeft : 'hidden ')}>
            <FontAwesomeIcon className="wishlist-ic" icon={faChevronLeft} />
          </div>
        }
        rightArrow={
          <div className={classNames(props?.showIcon ? TStyle.arrow : 'hidden')}>
            <FontAwesomeIcon className="wishlist-ic" icon={faChevronRight} />
          </div>
        }
      >
        {props?.renderItem}
      </Carousel>
    </div>
  )
}

export default memo(CarouselCustom)
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { memo } from "react";

interface IProps {
  item?: any;
}

const CoursesLink: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.item;
  return (
    <li>
      <a href={item?.link}>
        <FontAwesomeIcon icon={faAngleRight} style={{ marginRight: "5px" }} />
        {item?.title}
      </a>
    </li>
  );
};

export default memo(CoursesLink);

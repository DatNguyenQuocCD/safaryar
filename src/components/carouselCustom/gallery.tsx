import React, { useState, useEffect, useRef, memo } from "react";
import Slider from "react-slick";

interface IProps {
  images?: any;
}
const Gallery: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.images;
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  let sliderRef1 = useRef(null);
  let sliderRef2 = useRef(null);

  useEffect(() => {
    setNav1(sliderRef1);
    setNav2(sliderRef2);
  }, []);

  return (
    <div className="slider-container mb-[60px]">
      <Slider
        asNavFor={nav2}
        ref={(slider) => (sliderRef1 = slider)}
        arrows={false}
      >
        {item.map((item: any, index: number) => (
          <div key={index}>
            <img className="w-[1400px]" src={item?.img} />
          </div>
        ))}
      </Slider>
      <div className="px-[10px] py-[20px]">
        <Slider
          asNavFor={nav1}
          ref={(slider) => (sliderRef2 = slider)}
          slidesToShow={5}
          swipeToSlide={true}
          focusOnSelect={true}
          arrows={false}
          // autoplay={true}
          // autoplaySpeed={2000}
        >
          {item.map((item: any, index: number) => (
            <div className="cursor-pointer !flex justify-center" key={index}>
              <img src={item?.img} className="!w-[220px]" />
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default memo(Gallery);

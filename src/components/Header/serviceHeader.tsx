import { memo } from "react";
import StarRating from "../starRating";

interface IProps {
  item?: any;
}

const ServiceHeader: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.item;
  return (
    <div className="st-single-service-content">
      <div className="st-service-header2">
        <div className="left">
          <h1 className="st-heading">From Fes: {item?.title}</h1>

          <div className="sub-heading">
            <div className="d-inline-block d-sm-flex align-items-center">
              <div className="st-review-score">
                <div className="flex">
                  <div className="left mr-7">
                    <div className="star-sc">
                      <div className="review">
                        <StarRating rating={item?.star} />
                      </div>
                      {item?.star} <span>{item?.reviews} reviews</span>
                    </div>
                  </div>
                  <div className="supplier-name">
                    <span className="visibility-pixel"></span>
                    <small className="supplier-name__label">
                      Activity provider:
                    </small>
                    <a href="" className="eriment__link">
                      {` ${item?.location}`}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(ServiceHeader);

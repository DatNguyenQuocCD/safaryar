import Adventures from "../components/Adventures";
import BannerHome from "../components/Banner/Banner-home";
import ItemFood from "../components/ItemFood";
import CollectionHeader from "../components/ItemFood/collectionHeader";
import ItemSights from "../components/ItemFood/itemSights";
import Trending from "../components/Trending";
import CarouselCustom from "../components/carouselCustom";

import { AdventuresList, DbHomePage, TrendingList } from "../db/database";

export default function Home() {
  return (
    <div>
      <BannerHome />
      <div className="max-md:mx-[20px] max-lg:mx-[20px] max-w-[1320px] mx-auto mt-[60px]">
        <CollectionHeader
          cardP="London Exploration"
          cardH2="Based on your search in London"
        />
        <CarouselCustom
          show={3}
          slide={1}
          showIcon={true}
          renderItem={DbHomePage.searchSlider?.map(
            (item: any, index: number) => (
              <ItemFood key={index} item={item} />
            )
          )}
        />
        <CollectionHeader
          className="mt-[60px]"
          cardP="Go & Discover"
          cardH2="Unforgettable experiences around the world"
        />
        <div className="grid max-[1660px]:grid-cols-3 max-lg:grid-cols-2 max-md:grid-cols-1 grid-cols-4 gap-6">
          {DbHomePage.searchSlider?.map((item: any, index: number) => (
            <ItemFood key={index} item={item} />
          ))}
        </div>
        <CollectionHeader
          className="mt-[60px]"
          cardP="Top sights for you"
          cardH2="For you top sights can't miss"
        />
        <CarouselCustom
          className="mb-[60px]"
          show={4}
          slide={2}
          showIcon={true}
          renderItem={DbHomePage.sights.map((item: any, index: number) => (
            <ItemSights key={index} item={item} />
          ))}
        />
        <CollectionHeader
          cardP="Awe-Inspiring Adventures"
          cardH2="Awe-inspiring sports around the world"
        />
        <CarouselCustom
          className="mb-[60px]"
          show={4}
          slide={1}
          showIcon={true}
          renderItem={AdventuresList.map((item: any, index: number) => (
            <Adventures key={index} adventures={item} />
          ))}
        />
        <CollectionHeader
          cardP="Global Must-Sees"
          cardH2="Top attractions worldwide"
        />
        <div className="flex justify-center">
          <div className="grid sm:grid-cols-2 lg:grid-cols-4 gap-4 w-[1320px]">
            {TrendingList.map((item: any, index: number) => (
              <Trending key={index} trending={item} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

import { memo } from "react";
import Image from "next/image";

interface IProps {
  item?: any
}

const ItemSights: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.item

  return (
    <div className="sights-box">
      <a className="sight-card" href="">
        <figure className="image-figure">
          <Image src={item?.image} alt="" />
          <figcaption className="sight-card_caption">
            <div className="intro_center text-center">
              <h2>{item?.name}</h2>
              <h4>{item?.activities}</h4>
            </div>
          </figcaption>
        </figure>
      </a>
    </div>
  )
}

export default memo(ItemSights)
export const DbHomePage = {
  searchSlider: [
    {
      id: 1,
      image: require("../../public/images/d2.jpg"),
      name: "London: Jack the Ripper Museum Tickets",
      hours: 3,
      group: "Small group",
      star: 4.5,
      review: 1543,
      from: 3858,
    },
    {
      id: 2,
      image: require("../../public/images/d2.jpg"),
      name: "London: Jack the Ripper Museum Tickets",
      hours: 3,
      group: "Small group",
      star: 3.5,
      review: 1543,
      from: 3858,
    },
    {
      id: 3,
      image: require("../../public/images/d2.jpg"),
      name: "London: Jack the Ripper Museum Tickets",
      hours: 3,
      group: "Small group",
      star: 2.5,
      review: 1543,
      from: 3858,
    },
    {
      id: 4,
      image: require("../../public/images/d2.jpg"),
      name: "London: Jack the Ripper Museum Tickets",
      hours: 3,
      group: "Small group",
      star: 1,
      review: 1543,
      from: 3858,
    },
    {
      id: 5,
      image: require("../../public/images/d2.jpg"),
      name: "London: Jack the Ripper Museum Tickets",
      hours: 3,
      group: "Small group",
      star: 6,
      review: 1543,
      from: 3858,
    },
    {
      id: 6,
      image: require("../../public/images/d2.jpg"),
      name: "London: Jack the Ripper Museum Tickets",
      hours: 3,
      group: "Small group",
      star: 9.5,
      review: 1543,
      from: 3858,
    },
  ],
  sights: [
    {
      id: 1,
      image: require("../../public/images/d2.jpg"),
      name: "1. Statue of Liberty",
      activities: "144 Activities",
    },
    {
      id: 2,
      image: require("../../public/images/d2.jpg"),
      name: "1. Statue of Liberty",
      activities: "144 Activities",
    },
    {
      id: 3,
      image: require("../../public/images/d2.jpg"),
      name: "1. Statue of Liberty",
      activities: "144 Activities",
    },
    {
      id: 4,
      image: require("../../public/images/d2.jpg"),
      name: "1. Statue of Liberty",
      activities: "144 Activities",
    },
    {
      id: 5,
      image: require("../../public/images/d2.jpg"),
      name: "1. Statue of Liberty",
      activities: "144 Activities",
    },
    {
      id: 6,
      image: require("../../public/images/d2.jpg"),
      name: "1. Statue of Liberty",
      activities: "144 Activities",
    },
  ],
};

export const AdventuresList = [
  {
    id: 1,
    img: "images/dest-05.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
  {
    id: 2,
    img: "images/dest-06.jpg",
    title: "Greece",
    price: 1450,
    star: 4.5,
  },
  {
    id: 3,
    img: "images/dest-07.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
  {
    id: 4,
    img: "images/dest-08.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
  {
    id: 5,
    img: "images/dest-05.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
  {
    id: 6,
    img: "images/dest-06.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
  {
    id: 7,
    img: "images/dest-07.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
  {
    id: 8,
    img: "images/dest-08.jpg",
    title: "Mauritius",
    price: 1450,
    star: 4.5,
  },
];

export const TrendingList = [
  {
    id: 1,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 2,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 3,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 4,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 5,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 6,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 7,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 8,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 9,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 10,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 11,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
  {
    id: 12,
    img: "images/z1.jpg",
    title: "NIAGARA FALLS",
    price: 182,
    location: "madrid, spain",
  },
];

export const FooterLinkRight = [
  {
    id: 1,
    title: "About Us",
    link: "#",
  },
  {
    id: 2,
    title: "Careers",
    link: "#",
  },
  {
    id: 3,
    title: "Blog",
    link: "#",
  },
  {
    id: 4,
    title: "Press",
    link: "#",
  },
  {
    id: 5,
    title: "Gift Cards",
    link: "#",
  },
  {
    id: 6,
    title: "Explorer",
    link: "#",
  }
];

export const FooterLinkLeft = [
  {
    id: 1,
    title: "Contact",
    link: "#",
  },
  {
    id: 2,
    title: "Legal Notice",
    link: "#",
  },
  {
    id: 3,
    title: "Privacy Policy",
    link: "#",
  },
  {
    id: 4,
    title: "Cookies And Marketing Preferences",
    link: "#",
  },
  {
    id: 5,
    title: "General Terms And Conditions",
    link: "#",
  },
  {
    id: 6,
    title: "Information According To The Digital Services Act",
    link: "#",
  },
  {
    id: 7,
    title: "Sitemap",
    link: "#",
  },
  {
    id: 8,
    title: "Do Not Sell Or Share My Personal Information",
    link: "#",
  }
];


import { memo } from "react";

interface IProps {
  trending?: any;
}

const Trending: React.FC<IProps> = ({ ...props }: IProps) => {
  const item = props?.trending;
  return (
    <div className="col-span-1">
      <div className="trending_item clearfix">
        <div className="trending_image">
          <img src={item?.img} />
        </div>
        <div className="trending_content">
          <div className="trending_title">
            <a href="#">{item?.title}</a>
          </div>
          <div className="trending_price">From ${item?.price}</div>
          <div className="trending_location">{item?.location}</div>
        </div>
      </div>
    </div>
  );
};

export default memo(Trending);

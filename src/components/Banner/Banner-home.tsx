/* eslint-disable react/no-unescaped-entities */
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleRight,
  faMountainSun,
  faPersonRunning,
  faUtensils,
  faWandMagicSparkles,
} from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { faFortAwesome } from "@fortawesome/free-brands-svg-icons";

const BannerHome = () => {
  const [menu, setMenu] = useState(1);
  return (
    <div>
      <section className="banner-sc">
        <img className="banner-img" src="images/bg_1.jpg.webp" alt="" />

        <div className="banner-content flex justify-center">
          <div className="w-[1320px] banner-home">
            <div className="row">
              <div className="col-xl-6 col-lg-8">
                <h1>
                  Travel memories <br />
                  you'll never forget
                </h1>
                <div className="originals-badge flex">
                  <img src="images/og.png" />
                  <span>Originals by Safaryar</span>
                </div>
                <div className="hero-section__subheader">
                  Tour the enchanting sights of Reykjavik
                </div>

                <a href="" className="learnmore">
                  Learn more
                  <FontAwesomeIcon
                    icon={faAngleRight}
                    style={{ marginRight: "5px" }}
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div className="main-container">
        <div className="tab-container">
          <div className="flex justify-center">
            <ul
              className="nav nav-pills grid grid-cols-5 gap-4 w-[1320px] banner-home"
              id="pills-tab"
              role="tablist"
            >
              <li className="nav-item col-span-1" role="presentation">
                <a
                  href="#"
                  className={`nav-link ${menu === 1 ? "menu-active" : ""}`}
                  id="pills-foryou-tab"
                  onClick={() => {
                    setMenu(1);
                  }}
                >
                  <FontAwesomeIcon icon={faWandMagicSparkles} /> For you
                </a>
              </li>
              <li className="nav-item col-span-1" role="presentation">
                <a
                  href="#"
                  className={`nav-link ${menu === 2 ? "menu-active" : ""}`}
                  id="pills-culture-tab"
                  onClick={() => {
                    setMenu(2);
                  }}
                >
                  <FontAwesomeIcon icon={faFortAwesome} /> Culture
                </a>
              </li>
              <li className="nav-item col-span-1" role="presentation">
                <a
                  href="#"
                  className={`nav-link ${menu === 3 ? "menu-active" : ""}`}
                  id="pills-nature-tab"
                  onClick={() => {
                    setMenu(3);
                  }}
                >
                  <FontAwesomeIcon icon={faMountainSun} /> Nature
                </a>
              </li>
              <li className="nav-item col-span-1" role="presentation">
                <a
                  href="#"
                  className={`nav-link ${menu === 4 ? "menu-active" : ""}`}
                  id="pills-food-tab"
                  onClick={() => {
                    setMenu(4);
                  }}
                >
                  <FontAwesomeIcon icon={faUtensils} /> Food
                </a>
              </li>
              <li className="nav-item col-span-1" role="presentation">
                <a
                  href="#"
                  className={`nav-link ${menu === 5 ? "menu-active" : ""}`}
                  id="pills-food-tab"
                  onClick={() => {
                    setMenu(5);
                  }}
                >
                  <FontAwesomeIcon icon={faPersonRunning} /> Sports
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BannerHome;

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPhone,
  faClock,
  faAngleRight,
} from "@fortawesome/free-solid-svg-icons";
import {
  faFacebook,
  faTwitter,
  faInstagram,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons";
import { FooterLinkLeft, FooterLinkRight } from "../db/database";
import CoursesLink from "./CoursesLink";
/* eslint-disable react/no-unescaped-entities */
export default function Footer() {
  return (
    <footer className="footer-section px-[100px] lg:px-[50px]">
      <div className="footer-top flex justify-center">
        <div className="grid sm:grid-cols-2 lg:grid-cols-4 gap-4 w-[1320px]">
          <div className="col-span-1">
            <div className="widget company-intro-widget">
              <a href="index.html" className="site-logo">
                <img src="images/logo.png" alt="logo" />
              </a>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever.
              </p>
              <ul className="company-footer-contact-list">
                <li>
                  <FontAwesomeIcon
                    icon={faPhone}
                    style={{ fontSize: "20px", marginRight: "10px" }}
                  />
                  0123456789
                </li>
                <li>
                  <FontAwesomeIcon
                    icon={faClock}
                    style={{ fontSize: "20px", marginRight: "10px" }}
                  />
                  Mon - Sat 8.00 - 18.00
                </li>
              </ul>
              <ul className="social-links">
                <li>
                  <a href="#">
                    <FontAwesomeIcon icon={faFacebook} />{" "}
                  </a>
                </li>
                <li>
                  <a href="#">
                    <FontAwesomeIcon icon={faTwitter} />{" "}
                  </a>
                </li>
                <li>
                  <a href="#">
                    <FontAwesomeIcon icon={faInstagram} />{" "}
                  </a>
                </li>
                <li>
                  <a href="#">
                    <FontAwesomeIcon icon={faLinkedin} />{" "}
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-span-1">
            <div className="widget course-links-widget">
              <h5 className="widget-title">Popular Courses</h5>
              <ul className="courses-link-list">
                {FooterLinkLeft.map((item: any, index: number) => (
                  <CoursesLink key={index} item={item} />
                ))}
              </ul>
            </div>
          </div>
          <div className="col-span-1">
            <div className="widget course-links-widget">
              <h5 className="widget-title">Company</h5>
              <ul className="courses-link-list">
                {FooterLinkRight.map((item: any, index: number) => (
                  <CoursesLink key={index} item={item} />
                ))}
              </ul>
            </div>
          </div>
          <div className="col-span-1">
            <div className="widget newsletter-widget">
              <h5 className="widget-title">Newsletter</h5>
              <div className="footer-newsletter">
                <p>
                  Sign Up to Our Newsletter to Get Latest Updates & Services
                </p>
                <form className="news-letter-form">
                  <input
                    type="email"
                    name="news-email"
                    id="news-email"
                    placeholder="Your email address"
                  />
                  <input type="submit" value="Send" />
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom flex justify-center">
        <div className="w-[1320px]">
          <div className="flex justify-between">
            <div className="">
              <span className="copy-right-text">
                © 2024 <a href="">Safaryar</a> All Rights Reserved.
              </span>
            </div>
            <div className="flex">
              <ul className="terms-privacy flex">
                <li>
                  <a href="#">Terms & Conditions</a>
                </li>
                <li>
                  <a href="#">Privacy Policy</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}

import {
  faCalendarDay,
  faChevronDown,
  faGlobe,
  faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";

const ReservationBox = () => {
  const [modalParticipants, setModalParticipants] = useState<Boolean>(false);
  const [modalCalendar, setModalCalendal] = useState<Boolean>(false);
  const [participants, setParticipants] = useState<String>("");
  const [adult, setAdult] = useState<number>(1);
  const [children, setChildren] = useState<number>(1);
  const [senior, setSenior] = useState<number>(1);

  const [value, onChange] = useState(new Date());

  const [language, setLanguage] = useState<String>("");
  const [modalLanguage, setModalLanguage] = useState<Boolean>(false);

  const onChangePlus = (set: any, number: number) => {
    set(number + 1);
  };

  const onChangeMinus = (set: any, number: number) => {
    if (number > 1) {
      set(number - 1);
    }
  };

  const handleDateChange = (date: Date) => {
    onChange(date);
  };

  return (
    <div className="reservation-box-mn" id="check">
      <h2>Select participants, date, and language</h2>
      <div className="reserv-tabs">
        <div className="r-tab-on flex justify-between gap-5">
          <div className="r-tab-in">
            <div
              className="us-input-box"
              onClick={() => {
                if (modalParticipants) {
                  setModalParticipants(false);
                } else {
                  setModalParticipants(true);
                }
              }}
            >
              <div className="icon-left">
                <FontAwesomeIcon icon={faUsers} />
              </div>
              <input
                className="ba-input__label-text"
                value={`Adult x ${adult}, Children x ${children}, Senior x ${senior}`}
                title="Adult x 1"
                data-v-d099617c
                data-listener-added_fc0ee700="true"
              />
              <div className="icon-right">
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
            </div>
            <div
              className={`us-input-dropdown ${
                modalParticipants ? "" : "hidden"
              }`}
            >
              <div className="in-drop-flex">
                <div className="in-drop-on">
                  <h3>Adult</h3>
                  <span>(Age 13-61)</span>
                </div>
                <div className="in-drop-on">
                  <div className="quantity-box">
                    <button
                      className="quantity-button minus"
                      onClick={() => onChangeMinus(setAdult, adult)}
                    >
                      -
                    </button>
                    <input
                      type="text"
                      className="quantity-input"
                      value={`${adult}`}
                    />
                    <button
                      className="quantity-button plus"
                      onClick={() => onChangePlus(setAdult, adult)}
                    >
                      +
                    </button>
                  </div>
                </div>
              </div>

              <div className="in-drop-flex">
                <div className="in-drop-on">
                  <h3>Children</h3>
                  <span>(Age 12 and younger)</span>
                </div>
                <div className="in-drop-on">
                  <div className="quantity-box">
                    <button
                      className="quantity-button minus"
                      onClick={() => onChangeMinus(setChildren, children)}
                    >
                      -
                    </button>
                    <input
                      type="text"
                      className="quantity-input"
                      value={`${children}`}
                    />
                    <button
                      className="quantity-button plus"
                      onClick={() => onChangePlus(setChildren, children)}
                    >
                      +
                    </button>
                  </div>
                </div>
              </div>

              <div className="in-drop-flex">
                <div className="in-drop-on">
                  <h3>Senior</h3>
                  <span>(Age 62-99)</span>
                </div>
                <div className="in-drop-on">
                  <div className="quantity-box">
                    <button
                      className="quantity-button minus"
                      onClick={() => onChangeMinus(setSenior, senior)}
                    >
                      -
                    </button>
                    <input
                      type="text"
                      className="quantity-input"
                      value={`${senior}`}
                    />
                    <button
                      className="quantity-button plus"
                      onClick={() => onChangePlus(setSenior, senior)}
                    >
                      +
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div>
            <div
              className="us-input-box"
              onClick={() => {
                if (modalCalendar) {
                  setModalCalendal(false);
                } else {
                  setModalCalendal(true);
                }
              }}
            >
              <div className="icon-left">
                <FontAwesomeIcon icon={faCalendarDay} />
              </div>
              <input type="text" />
              <div className="icon-right">
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
            </div>
            <div
              className={`${
                modalCalendar ? "" : "hidden"
              } absolute z-50 w-[300px]`}
            >
              <Calendar onChange={() => handleDateChange} />
            </div>
          </div>
          <div>
            <div
              className="us-input-box"
              onClick={() => {
                if (modalLanguage) {
                  setModalLanguage(false);
                } else {
                  setModalLanguage(true);
                }
              }}
            >
              <div className="icon-left">
                <FontAwesomeIcon icon={faGlobe} />
              </div>
              <input
                type="text"
                id="selectedLanguage"
                placeholder="Select Language"
                value={`${language}`}
              />
              <div className="icon-right">
                <FontAwesomeIcon icon={faChevronDown} />
              </div>
            </div>
            <div
              className={`options ${modalLanguage ? "" : "hidden"}`}
              id="options"
            >
              <div className="option">
                <input type="radio" name="language" value="English" /> English
              </div>
              <div className="option">
                <input type="radio" name="language" value="French" /> French
              </div>
              <div className="option">
                <input type="radio" name="language" value="German" /> German
              </div>
              <div className="option">
                <input type="radio" name="language" value="Italian" /> Italian
              </div>
              <div className="option">
                <input type="radio" name="language" value="Portuguese" />{" "}
                Portuguese
              </div>
              <div className="option">
                <input type="radio" name="language" value="Spanish" /> Spanish
              </div>
            </div>
          </div>
        </div>
        <div className="mt-[30px]">
          <button id="check-avail" className="c-button" type="button">
            Check availability
          </button>
        </div>
      </div>
    </div>
  );
};

export default ReservationBox;

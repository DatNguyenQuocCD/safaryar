import { FC, memo } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar, faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';

interface StarRatingProps {
  rating: number;
}

const StarRating: FC<StarRatingProps> = ({ rating }) => {
  const stars = [];

  const integerPart = Math.floor(rating);
  const decimalPart = rating - integerPart;

  for (let i = 0; i < integerPart; i++) {
    stars.push(<FontAwesomeIcon key={i} icon={faStar} style={{ color: 'gold' }} />);
  }

  if (decimalPart > 0) {
    stars.push(<FontAwesomeIcon key="fractional" icon={faStarHalfAlt} style={{ color: 'gold' }} />);
  }

  return <div>{stars}</div>;
};


export default memo(StarRating);

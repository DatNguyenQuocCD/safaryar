import {
  faCartShopping,
  faHeart,
  faMagnifyingGlass,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";

export default function Header() {
  const router = useRouter();
  const isIndexPage = router.pathname === "/";
  return (
    <header className={`main-header ${isIndexPage ? "" : "oth"}`}>
      <div className="flex justify-center">
        <div className="flex justify-between w-[1320px]">
          <div>
            <div className="logo">
              <a href="">
                <img
                  src={`/images/${isIndexPage ? "logo.png" : "logosd.png"}`}
                  alt="logo"
                />
              </a>
            </div>
          </div>
          <div className="flex items-center max-[880px]:hidden">
            <div className="searchbox">
              <FontAwesomeIcon
                icon={faMagnifyingGlass}
                style={{ fontSize: "20px", padding: "0px 12px" }}
              />
              <input placeholder="Search Safaryar" type="text" />
              <button className="search-box-submit">Search</button>
            </div>
          </div>
          <div className="flex items-center justify-end">
            <div className="header-list">
              <ul>
                <li>
                  <FontAwesomeIcon icon={faHeart} />
                  <span> Wishlist</span>
                </li>
                <li>
                  <FontAwesomeIcon icon={faCartShopping} />
                  <span> Cart</span>
                </li>
                <li>
                  <FontAwesomeIcon icon={faUser} />
                  <span> Profile</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="max-[880px]:flex items-center hidden">
        <div className="searchbox">
          <FontAwesomeIcon
            icon={faMagnifyingGlass}
            style={{ fontSize: "20px", padding: "0px 12px" }}
          />
          <input placeholder="Search Safaryar" type="text" />
          <button className="search-box-submit">Search</button>
        </div>
      </div>
    </header>
  );
}

import { memo } from "react";

interface IProps {
  links?: any;
}

const Breadcrumbs: React.FC<IProps> = ({ ...props }: IProps) => {
  const items = props?.links;
  return (
    <nav aria-label="breadcrumb">
      <ol className="breadcrumb">
        {items.map((item: any, index: number) => (
          <li
            className="breadcrumb-item"
            key={index}
          >
            {index === items.length - 1 ? (
              <span>{item.title}</span>
            ) : (
              <a href="#">{item.title}</a>
            )}
          </li>
        ))}
      </ol>
    </nav>
  );
};

export default memo(Breadcrumbs);
